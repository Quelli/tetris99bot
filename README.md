# Tetris 99-Bot README #

### What is this project about? ###

* Tetris 99-Bot for Tetris 99 (Nintendo Switch)

### Code Style and workflow ###

* Using Python with [PEP-8](https://www.python.org/dev/peps/pep-0008/)

* Using a [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) (a little simplified)